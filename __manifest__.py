# -*- coding: utf-8 -*-
{
    'name': "Multi almacen ecommerce",

    'summary': """
        -Agregar Sucuarsales en el ecommerce.
        -Los pedidos de ventas se crean en con el location_id deacuerdo a el que halla seleccionado el usuario.

        """,

    'description': """
        Long description of module's purpose
    """,

    'author': "Jesus pozzo",
    'website': "http://www.mobilize.cl",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'web',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','web','website','website_sale','sale','delivery','website_sale_delivery'],
    'data': [
        'views/assets.xml',
        'views/templates.xml',
        'views/location_template.xml',
        'views/views.xml',
        'views/delivery_carrier.xml',
        'views/delivery_template.xml'
    ],

}