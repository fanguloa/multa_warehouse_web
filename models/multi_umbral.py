# -*- coding: utf-8 -*-

from odoo import models, fields, api
import logging

class multy_warehouse_web(models.Model):
    _name = 'product.multy_warehouse_web'
    _rec_name = "warehouse_id"

    warehouse_id = fields.Many2one('stock.warehouse',string ="Bodega")
    qty_umbral = fields.Integer(string ="Umbral")
    product_id = fields.Many2one('product.product',string = "Producto")
    qty_disponible = fields.Integer(string ="Cantidad Disponible",compute ="_get_qty_disponible")



    @api.depends('warehouse_id')
    def _get_qty_disponible(self):
        for field in self:
            locations = self.env['stock.location'].sudo().search([('usage','=','internal')]).filtered(lambda r: r.main_warehouse_id.id == field.warehouse_id.id)
            if locations:
                for ubic in locations:
                    total = 0			
                    stocks = self.env['stock.quant'].sudo().search([('product_id', '=', field.product_id.id),('location_id', '=', ubic.id)])
                    mano = 0
                    reservado = 0
                    for y in stocks:
                        mano+= y.quantity 
                        reservado+= y.reserved_quantity	
                    total += mano - reservado

                field.qty_disponible = total
