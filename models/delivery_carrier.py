# -*- coding: utf-8 -*-
from odoo import models, fields, api

class InheritDeliveryCarrier(models.Model):
    _inherit = "delivery.carrier"
    warehouse_id  = fields.Many2one('stock.warehouse', string='Sucursal')
  