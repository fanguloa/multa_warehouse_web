from odoo import models, fields, api

class Pricelist(models.Model):
    _inherit = 'product.pricelist'

    warehouse_id = fields.Many2one('stock.warehouse', string='Bodega')

    