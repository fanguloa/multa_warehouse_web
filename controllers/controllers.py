# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
import logging
from odoo.addons.website_sale.controllers.main import WebsiteSale as controller
_logger = logging.getLogger(__name__)

class MultyWarehouseWeb(http.Controller):
    @http.route([
        '''/shop/locations''',
        '''/shop/locations/<int:order_id>''',]
        , auth='public',website=True)
    def render_warehouses(self,order_id=None, **kw):
        logging.info(order_id)
        sale_order_id = None
        warehouse_ids = request.env['stock.warehouse'].sudo().search([])
        sale_order_id = request.env['sale.order'].sudo().browse([order_id])
      

        return request.render('multy_warehouse_web.warehouse_template', {
                            'warehouse_ids': warehouse_ids,
                            'order_id' :sale_order_id,
                            })

    @http.route(['''/shop/locations/update'''],
                auth="public",website = True
                )
    def update_warehouse(self,order_id=None,**kw):
        """
            Si existe una orden ya que solo trate de cambiar la sucursal en la cual quiere el usuario y limpia el carrto
            por el tema que si no hay stock en la otra sucursal.
        """
        if order_id:
            object_order = request.env['sale.order'].sudo().search([('id','=',order_id)])
            if object_order:
                object_order.sudo().write(
                    {   'order_line': [(5, 0, 0)],
                        'warehouse_id': kw['exampleRadios'],
                   })

        else:
            sale_order = request.website.sale_get_order(force_create=True)
            sale_order.warehouse_id = kw['exampleRadios']

        return request.redirect('/shop')

class WebsiteSale(controller):
    @http.route()
    def shop(self, page=0, category=None, search="", ppg=False, **post):
        res = super(WebsiteSale, self).shop(page, category, search, ppg, **post)
        pricelist_obj = request.env['product.pricelist'].sudo()
        order = request.website.sale_get_order()
        warehouse_id, new_pricelist = False, False
        if order:
            warehouse_id = order.warehouse_id.id
        if warehouse_id:
            new_pricelist = pricelist_obj.search([('warehouse_id', '=', warehouse_id)])
            res.qcontext.update({
            'pricelist': new_pricelist
                })
        return res


        
